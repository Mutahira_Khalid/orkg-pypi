ORKG Classes
============
Resources in the ORKG are typed to one or more of the ORKG's classes, in order to be able to access this information or manipulate it (i.e., add, edit, delete) a *classes* component is added to the ORKG class to encapsulate the actions.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the classes manager directly to do the following:

Getting class by ID
^^^^^^^^^^^^^^^^^^^
You can get certain class given that you know their ID value

.. code-block:: python

    ### Fetch class by id
    # id: the class id
    orkg.classes.by_id(id='C5')
    >>> (Success)
    {
       "id":"C5",
       "label":"MeasureProperty",
       "uri":"None",
       "created_at":"2020-05-11T14:52:34.316361+02:00",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"class"
    }

Getting classes by lookup
^^^^^^^^^^^^^^^^^^^^^^^^^
You can lookup a class by it's label. You can set a parameter to specify an exact match or a loose one.

.. code-block:: python

    ### Fetch a list of predicates
    # All parameters are optional
    # q: to search for a label containing
    # exact: to condition the search to as is
    orkg.classes.get_all(q='Property', exact=False)
    >>> (Success)
    [
       {
          "id":"C5",
          "label":"MeasureProperty",
          "uri":"None",
          "created_at":"2020-05-11T14:52:34.316361+02:00",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"class"
       },
       ...
    ]

Adding a new class
^^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new classes in the ORKG instance you are connected to.

*Note: if you have you credentials entered in the ORKG instance creation all newly added predicates will be credited to your user.*

.. code-block:: python

    ### Add a class
    # id: the id of the class (optional)
    # label: the label of the class
    # uri: the external uri that is equivalent to this class from an ontology or a taxonomy (optional)
    orkg.classes.add(id='Custom_ID', label='new class', uri='http://external-vocab.com')
    >>> (Success)
    {
       "id":"Custom_ID",
       "label":"new class",
       "uri":"http://external-vocab.com",
       "created_at":"2020-05-11T14:52:34.316361+02:00",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"class"
    }

Getting all resources of a certain class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can use the class manager of the ORKG python package to get resources of a certain class.

.. code-block:: python

    ### Get all resources that have a specific class
    # class_id: the class id to filter on
    # size: to specify the number of items in the page (Optional)
    # sort: to specify the key to sort on (Optional)
    # desc: to set the direction of sorting (Optional)
    # q: the term to filter the labels of the resources on (Optional)
    # exact: used in conjunction with "q" to specify an exact match or not (Optional)
    orkg.classes.get_resource_by_class(class_id='Paper', size=30, sort='label', desc=True, q='Paper', exact=False)
    >>> (Success)
    [
       {
          "id":"R1223",
          "label":"Paper 1",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
             "Paper"
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       ...
    ]


Updating an existing class
^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also update an existing class in the ORKG other than creating a new one. (***Use carefully***)

.. code-block:: python

    ### Updates a class
    # id (mandatory): the id of the class to change
    # label (mandatory): the new label of the class
    orkg.classes.update(id='Custom_ID', label='Coco')
    >>> (Success)
    {
       "id":"Custom_ID",
       "label":"Coco",
       "uri":"http://external-vocab.com",
       "created_at":"2020-05-11T14:52:34.316361+02:00",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"class"
    }

Check if a class exists
^^^^^^^^^^^^^^^^^^^^^^^
For your code to run smoothly you can check for the existence of classes before you update them for example. You can make sure that you code doesn't run into unexpected results.

.. code-block:: python

    ### Checks if class exists
    # id: the id of the class
    # returns a bool
    orkg.classes.exists(id='C1')
    >>> True
