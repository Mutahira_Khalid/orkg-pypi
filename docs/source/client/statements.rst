ORKG Statements
===============
Entities in the ORKG are connected to each other via predicates, the is struct of tiples <subject, predicate, object> is referred to as a statement, in order to be able to access this information or manipulate it (i.e., add, edit, delete) a *statements* component is added to the ORKG class to encapsulate the actions.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the statements manager directly to do the following:

Getting statement by ID
^^^^^^^^^^^^^^^^^^^^^^^
You can get certain statements given that you know their ID value

.. code-block:: python

    ### Fetch statement by id
    # id: the statement id
    orkg.statements.by_id(id='S5')
    >>> (Success)
    {
       "id":"S5",
       "subject":{
          "id":"R0",
          "label":"Gruber's design of ontologies",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       "predicate":{
          "id":"P0",
          "label":"addresses",
          "created_at":"2020-05-11T15:06:23.588208+02:00",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"predicate"
       },
       "object":{
          "id":"R8",
          "label":"Design of ontologies",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000"
    }

Getting statements
^^^^^^^^^^^^^^^^^^
You can get a list of statements. A variety of parameter can be passed to specify what order you need them in and how many results.

.. code-block:: python

    ### Get all statements
    # all the parameters are optional
    # size: to specify the number of items in the page
    # sort: to specify the key to sort on
    # desc: to set the direction of sorting
    orkg.statements.get(size=30, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by subject
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific subject

.. code-block:: python

    ### Get all statements by subject
    # subject_id is the subject to filter on
    # other parameters are optional
    orkg.statements.get_by_subject(subject_id='R0', size=30, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by predicate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific predicate

.. code-block:: python

    ### Get all statements by predicate
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_predicate(predicate_id='P0', size=20, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific object

.. code-block:: python

    ### Get all statements by object
    # object_id is the object to filter on
    # other parameters are optional
    orkg.statements.get_by_object(object_id='R8', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by subject and predicate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of statements that have a specific subject and predicate

.. code-block:: python

    ### Get all statements by subject and predicate
    # subject_id is the subject to filter on
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_subject_and_predicate(subject_id='R0', predicate_id='P0', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]


Getting statements by predicate and object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of statements that have a specific predicate and object.

.. code-block:: python

    ### Get all statements by object and predicate
    # object_id is the object to filter on
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_object_and_predicate(object_id='R8', predicate_id='P0', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]


Adding a new statement
^^^^^^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new statements in the ORKG instance you are connected to.

*Note: if you have you credentials entered in the ORKG instance creation all newly added statements will be credited to your user.*

.. code-block:: python

    ### Add a statement
    # subject_id: the id of the resource subject
    # predicate_id: the id of the predicate
    # object_id: the id of the resource, or literal to be placed at the object position
    orkg.statements.add(subject_id='R1', predicate_id='P2', object_id='L3')
    >>> (Success)
    {
       "id":"S32131",
       "subject":{
          "id":"R1",
          "label":"Some resource",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       "predicate":{
          "id":"P2",
          "label":"some predicate",
          "created_at":"2020-05-11T15:06:23.588208+02:00",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"predicate"
       },
       "object":{
          "id":"L3",
          "label":"some literal",
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"literal"
       },
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000"
    }

Updating an existing statement
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also update an existing statements in the ORKG other than creating a new one. (***Use carefully***)

.. code-block:: python

    ### Update a statement
    # not available on Labs yet
    # id: is the id of the statement to edit
    # other parameters are optional
    orkg.statements.update(id='S32131', subject_id='R1', predicate_id='P3', object_id='L3')
    >>> (Success)
    {
       "id":"S32131",
       "subject":{
          "id":"R1",
          "label":"Some resource",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       "predicate":{
          "id":"P3",
          "label":"some other predicate!",
          "created_at":"2020-05-11T15:06:23.588208+02:00",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"predicate"
       },
       "object":{
          "id":"L3",
          "label":"some literal",
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"literal"
       },
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000"
    }

Check if a statement exist
^^^^^^^^^^^^^^^^^^^^^^^^^^
For your code to run smoothly you can check for the existence of statements before you update them for example. You can make sure that you code doesn't run into unexpected results.

.. code-block:: python

    ### Checks if statement exists
    # id: the id of the statement
    # returns a bool
    orkg.statements.exists(id='S1')
    >>> True
