ORKG Objects
============

.. warning::

    Should be used by power-users only. This endpoint could have unaccounted consequences.


The package provides an endpoint to add new objects directly using the concepts defined on other components.

An object is a collection of resources, literals, and statements connecting them in a small sub-graph.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the objects manager directly to do the following:

Adding new object
^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new objects in the ORKG instance you are connected to.

*Note: if you have you credentials entered in the ORKG instance creation all newly added papers will be credited to your user.*

.. code-block:: python

    # First define the object content as dict (JSON object) (This is a sample of input)
    obj = {
        "predicates": [],
        "resource": {
            "name": "I rock maybe!",
            "classes": [
                "C2000"
            ],
            "values": {
                "P32": [
                    {
                        "@id": "R2"
                    }
                ],
                "P55": [
                    {
                        "label": "ORKG is so cool!",
                        "classes": [
                            "C3000"
                        ]
                    }
                ]
            }
        }
    }

    # Then add the content as an object to the ORKG instance
    orkg.objects.add(params=obj)
    >>> (Success)
    {
        "id": "R247004",
        "label": "I rock maybe!",
        "created_at": "2021-02-05T12:17:17.847889+01:00",
        "classes": [
            "C2000"
        ],
        "shared": 0,
        "created_by": "00000000-0000-0000-0000-000000000000",
        "_class": "resource",
        "observatory_id": "00000000-0000-0000-0000-000000000000",
        "extraction_method": "UNKNOWN",
        "organization_id": "00000000-0000-0000-0000-000000000000"
    }
