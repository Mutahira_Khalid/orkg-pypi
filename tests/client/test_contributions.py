from unittest import TestCase

import pandas as pd

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestContributions(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(ORKG, "simcomp_available", return_value=Fabricator.boolean_true())
    def test_check_simcomp(self, *args):
        self.assertTrue(self.orkg.simcomp_available)

    @mock_test(orkg.contributions.similar, return_value=Fabricator.success())
    def test_get_similar(self, *args):
        res = self.orkg.contributions.similar("R3005")
        self.assertTrue(res.succeeded)

    @mock_test(orkg.contributions.compare, return_value=Fabricator.success())
    def test_get_comparison(self, *args):
        res = self.orkg.contributions.compare(["R3005", "R1010"])
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.contributions.compare_dataframe, return_value=Fabricator.dataframe()
    )
    def test_get_comparison_df_without_metadata(self, *args):
        res = self.orkg.contributions.compare_dataframe(
            contributions=["R34499", "R34504"]
        )
        self.assertIsInstance(res, pd.DataFrame)

    @mock_test(
        orkg.contributions.compare_dataframe, return_value=Fabricator.dataframe()
    )
    def test_get_comparison_df_on_comparison_without_metadata(self, *args):
        res = self.orkg.contributions.compare_dataframe(comparison_id="R41466")
        self.assertIsInstance(res, pd.DataFrame)

    @mock_test(
        orkg.contributions.compare_dataframe,
        return_value=Fabricator.contributions.dataframe_with_metadata(),
    )
    def test_get_comparison_df_with_metadata(self, *args):
        res = self.orkg.contributions.compare_dataframe(
            contributions=["R34499", "R34504"], include_meta=True
        )
        self.assertIsInstance(res, tuple)
        self.assertTrue((res[0].columns == res[1].columns).all())
        self.assertTrue(
            "paper id" in res[1].index and "contribution id" in res[1].index
        )
        self.assertTrue(res[1].loc["contribution id"].is_unique)

    @mock_test(
        orkg.contributions.compare_dataframe,
        return_value=Fabricator.contributions.dataframe_with_metadata(),
    )
    def test_get_comparison_df_on_comparison_with_metadata(self, *args):
        res = self.orkg.contributions.compare_dataframe(
            comparison_id="R41466", include_meta=True
        )
        self.assertIsInstance(res, tuple)
        self.assertTrue((res[0].columns == res[1].columns).all())
        self.assertTrue(
            "paper id" in res[1].index and "contribution id" in res[1].index
        )
        self.assertTrue(res[1].loc["contribution id"].is_unique)
