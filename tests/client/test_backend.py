from unittest import TestCase

from orkg import ORKG
from orkg.common import Hosts
from tests import Fabricator, mock_test


class TestBackend(TestCase):
    @mock_test(ORKG, return_value=Fabricator.orkg.dummy_class())
    @mock_test(ORKG.__init__)
    @mock_test(ORKG, "token", return_value=Fabricator.orkg.dummy_token())
    def test_auth(self, *args):
        orkg = ORKG(host="http://127.0.0.1:8080", creds=("test@test.test", "test123"))
        self.assertIsNotNone(orkg.token)

    def test_successful_assignment_of_simcomp_host_automatically(self, *args):
        orkg = ORKG(host="https://sandbox.orkg.org/")
        self.assertIsNotNone(orkg.simcomp_host)

    def test_failed_assignment_of_simcomp_host_automatically(self, *args):
        orkg = ORKG(host="http://127.0.0.1:8080")
        self.assertIsNone(orkg.simcomp_host)

    def test_success_resolve_host_enum_sandbox(self, *args):
        orkg = ORKG(host=Hosts.SANDBOX)
        self.assertEqual(orkg.host, "https://sandbox.orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://sandbox.orkg.org/simcomp")

    def test_success_resolve_host_enum_production(self, *args):
        orkg = ORKG(host=Hosts.PRODUCTION)
        self.assertEqual(orkg.host, "https://orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://orkg.org/simcomp")

    def test_success_resolve_host_enum_incubating(self, *args):
        orkg = ORKG(host=Hosts.INCUBATING)
        self.assertEqual(orkg.host, "https://incubating.orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://incubating.orkg.org/simcomp")

    def test_mismatched_host_specification_has_no_effect(self, *args):
        # check if host/simcomp host name matches assigned host/simcomp host
        orkg = ORKG(
            host=Hosts.INCUBATING, simcomp_host="https://sandbox.orkg.org/simcomp"
        )
        self.assertEqual(orkg.host, "https://incubating.orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://sandbox.orkg.org/simcomp")

    def test_simcomp_host_cant_accept_hosts_raises_error(self, *args):
        with self.assertRaises(AttributeError):
            ORKG(simcomp_host=Hosts.SANDBOX)

    def test_typo_in_hosts_enum_raises_error(self, *args):
        with self.assertRaises(AttributeError):
            ORKG(host=Hosts.SANBOX)

    def test_using_hosts_enum_as_string_raises_error(self, *args):
        with self.assertRaises(ValueError):
            ORKG(host="Hosts.SANDBOX")

    def test_missing_http_in_str_host_raises_error(self, *args):
        with self.assertRaises(ValueError):
            ORKG(host="sandbox.orkg.org/")

    def test_missing_http_in_str_simcomp_host_raises_error(self, *args):
        with self.assertRaises(ValueError):
            ORKG(host=None, simcomp_host="sandbox.orkg.org/simcomp")

    def test_default_host_is_simcomp(self, *args):
        orkg = ORKG()
        self.assertEqual(orkg.host, "https://sandbox.orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://sandbox.orkg.org/simcomp")

    @mock_test(ORKG, "ping", return_value=Fabricator.boolean_true())
    def test_ping_is_True(self, *args):
        orkg = ORKG(host=Hosts.PRODUCTION)
        self.assertTrue(orkg.ping())

    @mock_test(ORKG, "ping", return_value=Fabricator.boolean_false())
    def test_ping_is_False(self, *args):
        orkg = ORKG(host="https://orkgtest.org/")
        self.assertFalse(orkg.ping())
