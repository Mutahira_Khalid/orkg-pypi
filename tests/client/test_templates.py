from unittest import TestCase

from orkg import OID, ORKG, Hosts
from tests import Fabricator, mock_test


class TestTemplates(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    def test_materialize(self, *args):
        self.orkg.templates.materialize_templates(
            templates=["R199091", "R199040"], verbose=False
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_df_template(self, *args):
        from pandas import DataFrame as df

        lst = ["this", "is", "fancy"]
        lst2 = [4, 2, 5]
        param = df(list(zip(lst, lst2)), columns=["word", "length"])
        self.orkg.templates.materialize_template(template_id="R288002")
        self.orkg.templates.test_df(
            label="what!", dataset=(param, "Fancy Table"), uses_library="pyORKG"
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_optional_param(self, *args):
        self.orkg.templates.materialize_template(template_id="R275249")
        self.orkg.templates.optional_param(label="what!", uses="pyORKG")
        self.orkg.templates.optional_param(
            label="wow!", uses="pyORKG", result="https://google.com"
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_recursive_templates(self, *args):
        self.orkg.templates.materialize_template(template_id="R48000")
        self.orkg.templates.problem(
            label="Test 1",
            sub_problem=self.orkg.templates.problem(
                label="Test 2",
                sub_problem=OID("R70197"),
                same_as="https://dumpy.url.again",
                description="This is a nested test",
            ),
            same_as="https://dumpy.url",
            description="This is a test",
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_template_cardinality_checks(self, *args):
        self.orkg.templates.materialize_template(template_id="R48000")
        self.orkg.templates.problem(
            label="Test 2",
            sub_problem=self.orkg.templates.problem(
                label="Test 2",
                sub_problem=OID("R70197"),
                same_as="https://dumpy.url.again",
                description=["More text also!!"],
            ),
            same_as="https://dumpy.url.again",
            description=["This is a nested test"],
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_template_with_list_values(self, *args):
        self.orkg.templates.materialize_template(template_id="R281240")
        self.orkg.templates.multi_param(
            label="multi_param",
            uses=["pyORKG", "rORKG"],
            description="This is a nested test",
        )
        self.assertTrue(True)
